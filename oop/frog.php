<?php

require_once 'animal.php';

class Frog extends Animal{
    
    // overriding menggunakan nama fungsi yang sama tapi mau mengubah isi atau kontenya.
    public function get_legs(){
        return '4'.$this->suara; 
    }
    public function jump(){
        echo "hop hop";
    }
}
?>